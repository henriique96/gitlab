export const COMMAND_PALETTE_COMMANDS = [
  {
    text: 'New project/repository',
    href: '/projects/new',
    keywords: ['new', 'project', 'repository'],
  },
  {
    text: 'New group',
    href: '/groups/new',
    keywords: ['new', 'group'],
  },
  {
    text: 'New snippet',
    href: '/-/snippets/new',
    keywords: ['new', 'snippet'],
  },
];
